pycsw-admin(1) - pycsw admin utility
====================================

##SYNOPSIS
`pycsw-admin` -c <command> -f <cfg> [-h] [-p /path/to/records] [-r]

##OPTIONS
    -c    Command to be performed:
              - setup_db
              - load_records
              - export_records
              - rebuild_db_indexes
              - optimize_db
              - refresh_harvested_records
              - gen_sitemap
              - post_xml
              - get_sysprof
              - validate_xml
              - delete_records

    -f    Filepath to pycsw configuration

    -h    Usage message

    -o    path to output file

    -p    path to input/output directory or file to read/write metadata records

    -r    load records from directory recursively

    -s    XML Schema

    -t    Timeout (in seconds) for HTTP requests (default is 30)

    -u    URL of CSW

    -x    XML document

    -y    force confirmation
    
##EXAMPLE
1) setup_db: Creates repository tables and indexes

	pycsw-admin -c setup_db -f default.cfg
        
2.) load_records: Loads metadata records from directory or file into repository

	pycsw-admin -c load_records -p /path/to/records -f default.cfg

    Load records from directory recursively

	pycsw-admin -c load_records -p /path/to/records -f default.cfg -r

    Load records from directory and force updates

        pycsw-admin -c load_records -p /path/to/records -f default.cfg -y

    Load metadata record from file into repository

        pycsw-admin -c load_records -p /path/to/file.xml -f default.cfg

3.) export_records: Dump metadata records from repository into directory

	pycsw-admin -c export_records -p /path/to/records -f default.cfg

4.) rebuild_db_indexes: Rebuild repository database indexes

	pycsw-admin -c rebuild_db_indexes -f default.cfg

5.) optimize_db: Optimize repository database

	pycsw-admin -c optimize_db -f default.cfg

6.) refresh_harvested_records: Refresh repository records
    which have been harvested

	pycsw-admin -c refresh_harvested_records -f default.cfg

7.) gen_sitemap: Generate XML Sitemap

	pycsw-admin -c gen_sitemap -f default.cfg -o /path/to/sitemap.xml

8.) post_xml: Execute a CSW request via HTTP POST

	pycsw-admin -c post_xml -u http://host/csw -x /path/to/request.xml

9.) get_sysprof: Get versions of dependencies

	pycsw-admin -c get_sysprof

10.) validate_xml: Validate an XML document against an XML Schema

	pycsw-admin -c validate_xml -x file.xml -s file.xsd

11.) delete_records: Deletes all records from repository

        pycsw-admin -c delete_records -f default.cfg

12.) delete_records: Deletes all records from repository without prompting

        pycsw-admin -c delete_records -f default.cfg -y

##MORE INFORMATION

More information about pycsw can be obtained from _/usr/share/doc/pycsw-doc_ after installing the *pycsw-doc* package.
