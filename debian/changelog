pycsw (2.6.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add python3-pkg-resources to (build) dependencies.
  * Fix gen_html.py syntax error.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 23 Feb 2025 06:23:11 +0100

pycsw (2.6.2+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * Update apache wsgi conf file to solve numpy import issue.

  [ Bas Couwenberg ]
  * New upstream release.
  * Add Rules-Requires-Root to control file.
  * Update lintian overrides.
  * Bump Standards-Version to 4.7.0, no changes.
  * Bump debhelper compat to 13.
  * Don't override dh_auto_test for custom pybuild system.
  * Don't override dh_auto_install.
  * Use execute_after instead of override in rules file.
  * Enable Salsa CI.
  * Remove unused rules.
  * Switch to dh-sequence-*.
  * Refresh patches.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 22 Feb 2025 18:15:52 +0100

pycsw (2.6.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * No-change rebuild for binary upload.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 22 Oct 2021 10:28:34 +0200

pycsw (2.6.1+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * New upstream release.
  * Fixed database path in default.cfg.
  * Fixed test generator config paths.
  * Set PYCSW_CONFIG in wsgi config file.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.0, no changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Drop undefined substitution variables.
  * Update doc-base paths.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 15 Oct 2021 08:43:01 +0200

pycsw (2.6.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump watch file version to 4.
  * Update lintian overrides.
  * Bump Standards-Version to 4.5.1, no changes.
  * Update copyright file.
  * Drop python3-six from (build) dependencies.
  * Drop patches applied/included upstream. Refresh remaining patches.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 05 Dec 2020 14:57:22 +0100

pycsw (2.4.2+dfsg-6) unstable; urgency=medium

  * Team upload.
  * No-change rebuild.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 27 Jul 2020 19:41:16 +0200

pycsw (2.4.2+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Switch libapache2-mod-wsgi dependency to libapache2-mod-wsgi-py3.
    (closes: #966322)
  * Add lintian override for embedded-javascript-library.
  * Mark patches as Forwarded: not-needed.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Jul 2020 20:18:47 +0200

pycsw (2.4.2+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Add upstream patch to fix FTBFS with sphinx 2.4.
  * Reinstate pycsw-doc package.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 06 May 2020 06:16:30 +0200

pycsw (2.4.2+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Drop pycsw-doc package, FTBFS with sphinx 2.4.
    (closes: #959775)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 05 May 2020 12:00:43 +0200

pycsw (2.4.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix SyntaxWarning issues with Python 3.8.
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
  * Don't rename manpages, not needed for ruby-ronn 0.9.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 11 Apr 2020 17:06:35 +0200

pycsw (2.4.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.1, no changes.
  * Drop Name field from upstream metadata.
  * Refresh patches.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 18 Jan 2020 08:07:40 +0100

pycsw (2.4.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Refresh patches.
  * Drop removal of empty directories removed upstream.
  * Add patch to not require exact versions of dependencies.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Sep 2019 07:38:37 +0200

pycsw (2.4.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.4.0, no changes.
  * Switch to Python 3 only.
  * Drop obsolete Breaks/Replaces.
  * Drop obsolete transitional package.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 13 Jul 2019 22:45:08 +0200

pycsw (2.4.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Update gbp.conf to use --source-only-changes by default.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 07 Jul 2019 09:56:44 +0200

pycsw (2.4.0+dfsg-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Make ronn generated manpage renaming more robust.
  * Remove package name from lintian overrides.
  * Drop python-pycsw-doc transitional package.
    (closes: #926569)
  * Update copyright years for Tom Kralidis.
  * Drop patches applied upstream, refresh remaining patches.
  * Add apipkg & pytest build dependencies.
  * Remove additional empty directories.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 17 May 2019 21:18:16 +0200

pycsw (2.2.0+dfsg-6) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.3.0, no changes.
  * Fix man page installation.
    (closes: #922995)

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 22 Feb 2019 21:42:35 +0100

pycsw (2.2.0+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 16:46:08 +0200

pycsw (2.2.0+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Refresh patches.
  * Add patch to fix SyntaxError with Python 3.7 by
    renaming 'async' property to 'asynchronous'.
    (closes: #903784)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Jul 2018 09:59:41 +0200

pycsw (2.2.0+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.1.5, no changes.
  * Drop obsolete get-orig-source target.
  * Drop ancient X-Python-Version field.
  * Strip trailing whitespace from control & rules files.
  * Update ronn build dependency.
  * Remove documentation outside usr/share/doc.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 Jul 2018 22:57:06 +0200

pycsw (2.2.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add patch to not use strict dependency on python-six.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 02 Apr 2018 17:35:25 +0200

pycsw (2.2.0+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Change priority from extra to optional.
  * Bump Standards-Version to 4.1.3, changes: priority.
  * Strip trailing whitespace from changelog.
  * Update copyright-format URL to use HTTPS.
  * Update watch file to use HTTPS.
  * Drop obsolete lintian override.
  * Add Disclaimer to copyright file.
  * Update Vcs-* URLs for Salsa.
  * Add overrides for dependency-on-python-version-marked-for-end-of-life.
  * Remove empty directories.

  [ Johan Van de Wauw ]
  * New upstream version 2.2.0+dfsg
  * Refresh patches
  * Adjust renamed document file
  * Update copyright years
  * Refresh patches/Remove obsolete patches
  * Add python-mock as build-dependency
  * Update my mailaddress

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 02 Apr 2018 13:24:40 +0200

pycsw (2.0.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Fix case in description.
  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 18 Jun 2017 22:04:03 +0200

pycsw (2.0.3+dfsg-1~exp1) experimental; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * New upstream release.

  [ Bas Couwenberg ]
  * Update copyright years for Tom Kralidis.
  * Refresh patches.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 05 Jun 2017 21:38:23 +0200

pycsw (2.0.2+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Add patch to fix desktop file validation errors.

  [ Angelos Tzotsos ]
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 18 Oct 2016 22:09:20 +0200

pycsw (2.0.1+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * New upstream release.
  * Removed pycsw-root patch after being included in 2.0.1.

  [ Bas Couwenberg ]
  * Refresh patches.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 17 Sep 2016 18:44:05 +0200

pycsw (2.0.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 19 Jul 2016 21:30:07 +0200

pycsw (2.0.0+dfsg-1~exp3) experimental; urgency=medium

  * Team upload.
  * Restructure binary packages, changes:
    - Make pycsw the main binary package, containing pycsw-admin,
      default-sample.cfg & examples.
    - Drop 'python-' prefix from -doc & -wsgi packages,
      turn old packages into transitional packages.
    - Use dh_apache2 to install pycsw.conf in /etc/apache2/conf-available,
      don't enable it via maintainer scripts.
  * Also build pycsw module for Python 3.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 13 Jul 2016 14:23:06 +0200

pycsw (2.0.0+dfsg-1~exp2) experimental; urgency=medium

  * Team upload.
  * Add patch to support overriding PYCSW_ROOT via environment variable.
    Fixes path to config files for tests installed by wsgi package.
  * Fix use jquery.js from libjs-jquery package.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 12 Jul 2016 01:01:32 +0200

pycsw (2.0.0+dfsg-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update pycsw-admin manpage for upstream changes.
  * Include csw.py in python-pycsw-wsgi again.
    Was removed in 2.0.0~rc1+dfsg-1~exp1.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 11 Jul 2016 19:09:35 +0200

pycsw (2.0.0~rc1+dfsg-1~exp2) experimental; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * Updated project description from upstream.

  [ Bas Couwenberg ]
  * Collapse double spaces in package descriptions,
    wrap lines at 75 characters.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 07 Jul 2016 03:09:31 +0200

pycsw (2.0.0~rc1+dfsg-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release candidate.
  * Update copyright file, changes:
    - Update copyright years
    - Add copyright holders
    - Change license shortname from xmit to Expat
    - Add license & copyright for documentation
  * Update patches, changes:
    - Drop 0003-*.patch, obsolete.
    - Drop 0007-Use-geolinks-0.2.0.patch, included upstream.
    - Refresh remaining patches.
  * Use dh_installdocs to install doc-base file.
  * Override dh_install to use --list-missing.
  * Update WSGI script to install.
  * Add python-six & python-xmltodict to dependencies.
  * Update default.cfg for database rename.
  * Override dh_auto_test to use `paver test`.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 07 Jul 2016 02:54:23 +0200

pycsw (1.10.4+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Add upstream metadata.

  [ Angelos Tzotsos ]
  * Added upstream 1.10.x patch to use geolinks 0.2.0.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 10 Jun 2016 19:32:16 +0200

pycsw (1.10.4+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop libxml2-xee.patch, applied upstream. Refresh remaining patches.
  * Enable parallel builds.
  * Bump Standards-Version to 3.9.8, no changes.
  * Refresh patches.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 29 Apr 2016 23:01:31 +0200

pycsw (1.10.1+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Angelos Tzotsos ]
  * Fixed libxml < 2.9 XEE vulnerability.

  [ Bas Couwenberg ]
  * Include changes by Angelos from OSGeo-Live package.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 27 Jun 2015 15:36:06 +0200

pycsw (1.10.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 1.10.1+dfsg
  * Fix description (wsgi instead of cgi)
  * Update patches
  * Don't include external references to images

 -- Johan Van de Wauw <johan.vandewauw@gmail.com>  Sat, 20 Jun 2015 22:11:36 +0200

pycsw (1.10.0+dfsg-1) unstable; urgency=medium

  * Initial upload (closes: #762145), targeting non-free after ftp-masters
    comments

 -- Johan Van de Wauw <johan.vandewauw@gmail.com>  Sun, 07 Dec 2014 09:08:21 +0100
